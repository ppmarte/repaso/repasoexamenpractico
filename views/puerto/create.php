<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Puerto $model */

$this->title = 'Create Puerto';
$this->params['breadcrumbs'][] = ['label' => 'Puertos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="puerto-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
